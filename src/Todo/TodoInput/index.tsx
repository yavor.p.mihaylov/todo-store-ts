import TodoStore from "../../stores/TodoStore";
import React, { ChangeEvent, FormEvent, useState } from "react";
import styles from './TodoInput.module.css';
import { useStore } from "../../stores";

const TodoInput = () => {    
    const {todos} = useStore()
    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
        const formElement = e.target as HTMLFormElement;
        const formData = new FormData(formElement);
        const value = String(formData.get("todo-input") || "");
        todos.add(value);
        formElement.reser();        
    }
    return (
        <form onSubmit={handleSubmit} className={styles["todo-input-group"]}>        
            <input  name="todo-input" placeholder="Add Todo..."  />
            <button type="submit">Add Todo</button>
        </form>
    )
}

export default TodoInput; 

